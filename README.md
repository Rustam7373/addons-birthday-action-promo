# Module: birthday action promo #

Adds a new condition to create a promotion on the buyer's birthday

### In detail ###

Designed for CMS CS-Cart

### How to install and configure the module? ###

To install, unload the module into the CS-Cart root directory.

### Dependencies ###

Does not work without the module "Age verification"